extern crate bitreader as br;

use std::env;
use std::fs::File;
use std::io::{BufReader, Read, Seek, SeekFrom};
use std::io::stdin;

fn main() {
  let mut bytes = Vec::new();

  {
    let mut file = BufReader::new(File::open(env::args().skip(1).next().unwrap()).unwrap());
    // First 1024 bytes are the header and we don't need them for anything at the moment.
    file.seek(SeekFrom::Current(1024)).unwrap();
    file.read_to_end(&mut bytes).unwrap();
  }

  let mut bits = br::BitReader::new(&bytes);
  let mut min_label = 180;  // rougly tengen
  let mut max_label = 180;

  let mut _line = String::new();
  while bits.position() < 8 * bytes.len() as u64 {
    let position = bits.position();
    let tag = bits.read_u16(16).unwrap();  // b"GO"
    if tag == 0x474f {
      // println!("position: {}+{}", position / 8, position % 8);
      // println!("tag: {}", tag);
      let mut label = bits.read_u32(8).unwrap();
      label |= bits.read_u32(8).unwrap() << 8;
      label |= bits.read_u32(8).unwrap() << 16;
      label |= bits.read_u32(8).unwrap() << 24;
      let mut current_player_plane = vec![0u8; 361];
      let mut opposite_player_plane = vec![0u8; 361];
      let mut ko_plane = vec![0u8; 361];
      let mut move_plane = vec![0u8; 361];
      move_plane[label as usize] = 1;
      for _ in 0..3 {
        for it in 0..361 {
          current_player_plane[it] |= bits.read_u8(1).unwrap();
        }
      }
      for _ in 0..3 {
        for it in 0..361 {
          opposite_player_plane[it] |= bits.read_u8(1).unwrap();
        }
      }

      for it in 0..361 {
        ko_plane[it] |= bits.read_u8(1).unwrap();
      }

      // Examples are byte-padded
      while !bits.is_aligned(1) {
        bits.skip(1).unwrap();
      }

      // for it in 0..361usize {
      //   if it % 19 == 0 {
      //     println!();
      //   }

      //   if it == 0 {
      //     println!("label: {}", label);
      //   }

      //   if move_plane[it] != 0 {
      //     print!("#");
      //   } else if current_player_plane[it] != 0 {
      //     print!("X");
      //   } else if opposite_player_plane[it] != 0 {
      //     print!("O");
      //   } else {
      //     print!(" ");
      //   }
      // }
      // println!("\n=================");
      if label < min_label {
        min_label = label;
      }
      if label > max_label {
        max_label = label;
      }
    } else if tag == 0x454e {
      let end_byte = bits.read_u8(8).unwrap();
      if end_byte == 0x44 {
        break;
      } else {
        panic!("bad end byte: {:02x}", end_byte);
      }
    } else {
      panic!("unknown tag: {:04x}", tag);
    }
  }

  println!("min: {}, max: {}", min_label, max_label);
}
